### Components config selection
## More info on https://gitlab.com/oxkhar/makeup/blob/master/example/Makefile
##
# Docker (choose one... )
#  values: docker-compose | docker-engine (default)
# MKUP_HAS += docker-engine
##
# Merge release (choose one... )
#  values:  git-flow | git-trunk (default)
MKUP_HAS += git-trunk
##
# PHP options...
#  values: php-composer php-build
#          php-qa
#          php-doctrine php-symfony
#          php-tests-xunit php-tests-mutant php-tests-rspec php-tests-bdd
# MKUP_HAS += php-composer php-tests-xunit php-qa

### Environment configuration...
##
# PHP_CLI_ENGINE = LOCAL # For local installation
# PHP_CLI_ENGINE = DOCKER # To use with PHP Docker image
# PHP_CLI_ENGINE = DOCKER_COMPOSE # Project is defined to execute in a Docker Compose file
##
# QA config...
# params: QA_MODULES | QA_MODULES_DISABLE
# values: codesniffer copy_paste_detector cs_fixer insights loc mess_detector metrics phpstan
# QA_MODULES_DISABLE =
##
# params: QA_ANALYSE | QA_ANALYSE_DISABLED
# values: phpstan php-cs-fixer phpcs phploc phpmd phpcpd phpinsights
# QA_ANALYSE_DISABLED = phpmd
##
# params: QA_REPORTS | QA_REPORTS_DISABLED
# values: phpstan-report php-cs-fixer-report phpcs-report phpmd-report phpcpd-report phploc-report phpmetrics
# QA_REPORTS_DISABLED =
#
# PHPSTAN_OPTIONS  := --memory-limit=256M

### Config example, needed for some components
# PHP_COMPOSER_VERSION = 2.0.12
# DOCKER_COMPOSE_VERSION = 1.29.2
# DATA_DIRS = # sqlite postgresql mysql
# UNINSTALL_FILES += # .env.local.php

### Tag rules to update files with new version
TAG_APP_FILES += ./.env
./.env.tag: TAG_REG_EXP=/APP_VERSION=.*/APP_VERSION=${TAG}/

###
MKUP_INSTALL_PATH = .makeUp
MKUP_URL_INSTALL = https://gitlab.com/oxkhar/makeup/-/raw/master/makeup.sh
$(shell [ ! -e "${MKUP_INSTALL_PATH}" ] && (curl -sSL "${MKUP_URL_INSTALL}" | bash -s install "${MKUP_INSTALL_PATH}"))
include ${MKUP_INSTALL_PATH}/MakeUp.mk
