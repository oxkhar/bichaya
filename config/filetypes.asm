# For complete documentation of this file, please see Geany's main documentation

[styling]
# Edit these in the colorscheme .conf file instead
default=default
comment=comment_line
commentblock=comment
commentdirective=comment
number=number_1
string=string_1
operator=operator
identifier=identifier_1
cpuinstruction=keyword_1
mathinstruction=keyword_2
register=type
directive=preprocessor
directiveoperand=keyword_3
character=character
stringeol=string_eol
extinstruction=keyword_4

[keywords]
# all items must be in one line
# this is by default a very simple instruction set; not of Intel or so
instructions=add addst and bit call ccf cli cp cpd cpdr cpi cpir cpl daa dec dek di div divst djnz ei ex exx halt hlt inc ink jez jgz jlz jmp jp jr jsr lad ld ldd lddr ldi ldir lds ldx lia lsa mul mulst neg nop or otdr otir outd outi pop popac push pushac res ret reti retn rla rlca rld rra rrca rrd rst scf set sla sra srl sll spi sub subst swap xor
registers=a d e b c h l af hl bc de ix iy sp pc r i
directives=org list nolist page equivalent word text include incbin ds dw db macro proc rept irp if else endif endm exitm end endp equ local public .shift


[settings]
# default extension used when saving files
extension=asm

# the following characters are these which a "word" can contains, see documentation
#wordchars=_abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789

# single comments, like # in this file
comment_single=;
# multiline comments
#comment_open=
#comment_close=

# set to false if a comment character/string should start at column 0 of a line, true uses any
# indentation of the line, e.g. setting to true causes the following on pressing CTRL+d
   #command_example();
# setting to false would generate this
#   command_example();
# This setting works only for single line comments
comment_use_indent=true

# context action command (please see Geany's main documentation for details)
context_action_cmd=

[indentation]
#width=4
# 0 is spaces, 1 is tabs, 2 is tab & spaces
type=0

[build_settings]
# %f will be replaced by the complete filename
# %e will be replaced by the filename without extension
# (use only one of it at one time)
compiler=pasmo "%f"

[build-menu]
FT_00_LB=_Compilar
FT_00_CM=pasmo -v "%f" "%e.bin" "%e.sys" "%e.pub"
FT_00_WD=
EX_00_LB=_Ejecutar
EX_00_CM=RetroVirtualMachine -b=cpc6128@es -ns -jump=0x4000  -load=0x4000 "%e.bin"
EX_00_WD=
EX_01_LB=Cargar
EX_01_CM=RetroVirtualMachine -b=cpc6128@es -ns  -load=0x4000 "%e.bin"
EX_01_WD=
error_regex=ERROR*
