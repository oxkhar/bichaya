
initEntities:
    jp bichayaCreate

initScreen:
    ld hl, 0xC000
    ld bc, 0x50C8
    xor a

    jp drawByte  ; Erase all screen

keymap:
    DS 10  ; map with 10*8 = 80 key status bits (bit=0 key is pressed)

keyboardScan:
    di               ;1
    ld hl, keymap    ;3

    ld bc, 0xF782    ;3
    out [c], c       ;4

    ld bc, 0xF40E    ;3
    ld e, b          ;1
    out [c], c       ;4

    ld bc, 0xF6C0    ;3
    ld d, b          ;1
    out [c], c       ;4
    xor a            ;1
    out [c], a       ;4

    ld bc, 0xF792    ;3
    out [c], c       ;4

    ld a, 0x40       ;2
    ld c, 0x4a       ;2 43
_keyboardScanLoop:
    ld b, d         ;1
    out [c], a      ;4 select line

    ld b, e         ;1
    ini             ;5 read bits and write into KEYMAP

    inc a           ;1
    cp c            ;1
    jr c, _keyboardScanLoop       ;2/3 9*16+1*15=159

    ld bc, #F782    ;3
    out [c], c      ;4

    ei              ;1 8 =210 microseconds
    ret
