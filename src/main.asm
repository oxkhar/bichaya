
;
INCLUDE "lib/headers.asm"
INCLUDE "ogm/headers.asm"
INCLUDE "obj/headers.asm"

DEBUG DEFL true

ORG 0x4000

init:
    call disableFirmware

main:
    call initEntities
    call waitVSync
    halt
    halt
    halt
    call initScreen

loop:
    colorBorder 11
    call keyboardScan
    colorBorder 12
    updateEntities

    colorBorder 04
;;  Tick, tack...
    timerTickTack
    colorBorder 04, 3

    colorBorder 7
    call entityClearFromScreen

    colorBorder 8
    drawEntities

    colorBorder 04, 1

    jp loop


; #####

INCLUDE "lib/include.asm"
INCLUDE "ogm/include.asm"
INCLUDE "obj/include.asm"

END 0x4000
