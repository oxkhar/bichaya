

; ######
bichayaCreate:
    ld ix, bichaya

    ldxy hl, 10, 80
    ld (ix + entityHexScreenH), h
    ld (ix + entityHexScreenL), l
    ld (ix + entityPosX), 10
    ld (ix + entityStatus), 0x00

    stackAddItem stackPlayer
;   jp bichayaWait  // No needed, it's the next

bichayaWait:
    ld (ix + entityFncUpdateH), HIGH bichayaWaiting
    ld (ix + entityFncUpdateL), LOW bichayaWaiting
    set bichayaStatusWaiting, (ix + entityStatus)

    jumpInitializeEntityWithSprite bichayaSprite, bichayaSpeedAnimation

bichayaWaiting:
    call spriteUpdateAnimation

    ld hl, waitingCounter
    inc (hl)

    ld a, (waitingCounter)
    cp 0xAF
    call z, bichayaMove

    ret


bichayaMove:
    res bichayaStatusWaiting, (ix + entityStatus)
    set bichayaStatusMoving, (ix + entityStatus)
    set entityStatusIsDirty, (ix + entityStatus)

    ld (ix + entityFncUpdateH), HIGH bichayaMoving
    ld (ix + entityFncUpdateL), LOW bichayaMoving

    nextFrameInTime motion

    ret

bichayaMoving:
    call spriteUpdateAnimation

    ld a, (ix + entityPosX)
    cp  0x40                 ; Is the middle of scenario?
    ret z

    ld a, (timer)
    cp (ix + motionFrame)    ; Check if it's the time to move
    ret nz                   ;
    nextFrameInTime motion   ; calculate next frame to move

    ; Update position
    call entityEraseSprite

    ;; Move to left
    call moveRightOne

    ret

; IX =>
; Destroy: HL, DE
bichayaDestroy:
    ld (ix + entityStatus), 0x00
    set entityStatusIsDestroyed, (ix + entityStatus)

    call entityEraseSprite

    stackRemoveItem stackPlayer

    ret
