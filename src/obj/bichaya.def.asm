
;;
bichayaStatusWaiting EQU 3
bichayaStatusMoving  EQU 4
;;

;;
bichayaSpeedMotion EQU 0x06
bichayaSpeedAnimation EQU 0x08

;;
waitingCounter:
    db 0x00

;;
stackCreate stackPlayer, 1

entityWithAnimationAndMotion bichaya, 10, 200, 1, 0, bichayaSpeedMotion, bichayaSpeedAnimation, bichayaSprite, void
bichayaStruct:
    DB 0x00 ;

spriteStruct bichayaSprite, 3, 2, 8, animateSpriteBounceLoop, drawImage
