
INCLUDE "lib/test_case_entity.asm"

testEntity test_entity

test_entity:
    call test_sprite_one
    call test_sprite_two
    call test_tank_action
    jp test_entity

test_sprite_one:
    testSprite entity, 10, 80, spriteOne, spriteOneSpeed
    ret

test_sprite_two:
    testSprite entity, 10, 80, spriteTwo, spriteTwoSpeed
    ret


test_entity_action:
    ld hl, testActionCounter
    ld (hl), 0x08

_testEntityInit:
    ldxy hl, 10, 130
    ld bc, 0x0f82
    call entityCreate

_testEntityLoop:

    stackWalker entityUpdate, stackEntity, callEntityUpdateProcess

    ; ...
    timerTickTack
    halt
    halt

    colorBorder 05
    call entityClearFromScreen
    colorBorder 06
    stackWalker entityDraw, stackEntity, drawSpriteIsDirty
    colorBorder 04


    ;; ... Proccess to change status and continue with loop
    ;; ... if end of status finalize enotity

    call entityDestroy

    ld hl, testActionCounter
    dec (hl)
    jp nz, _testEntityInit

    ret

testActionCounter:
    DB 0x00

testCaseEntityEnd

;;
INCLUDE "obj/entity.inc.asm"
INCLUDE "lib/test_case_end.asm"
