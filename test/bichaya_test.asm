
INCLUDE "lib/test_case_entity.asm"

testEntity test_bichaya

test_bichaya:
    call test_bichaya_wait
    call test_bichaya_action
    jp test_bichaya

test_bichaya_wait:
    testSprite bichaya, 15, 40, bichayaSprite, 0x0A
    ret


test_bichaya_action:
    ld hl, testActionCounter
    ld (hl), 0x08

_testBichayaInit:
    call bichayaCreate

_testBichayaLoop:

    stackWalker bichayaUpdate, stackPlayer, callEntityUpdateProcess

    timerTickTack
    halt
    halt

    colorBorder 05
    call entityClearFromScreen
    colorBorder 06
    stackWalker bichayaDraw, stackPlayer, drawSpriteIsDirty
    colorBorder 04

    ld ix, bichaya
    ld a, (ix + entityPosX)
    cp  0x40                 ; Is the middle of scenario?
    jp nz, _testBichayaLoop

    call bichayaDestroy

    ld hl, testActionCounter
    dec (hl)
    jp nz, _testBichayaInit

    ret
testActionCounter:
    DB 0x00


testCaseEntityEnd

INCLUDE "obj/bichaya.inc.asm"
INCLUDE "lib/test_case_end.asm"
